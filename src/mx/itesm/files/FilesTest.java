/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.files;

import java.io.*;

/**
 *
 * @author L03023498
 */
public class FilesTest {
    
    public static void main(String[] args) {
        
        String path = "src/mx/itesm/files/data.txt";
        
        System.out.println("Current folder:" + 
                System.getProperty("user.dir"));
        
        System.out.println("Looking for " + path);
        File file = new File(path);
        if (file.exists()) {
            System.out.println(path + " was found");
            
            if (file.isDirectory()) {
                System.out.println("This is a folder");
                
                String[] contents = file.list();
                for (String element : contents) {
                    System.out.println(element);
                }
            }
            
            if (file.isFile()) {
                System.out.println("This is a file");
                
                // Leemos el archivo
                try {
                    BufferedReader input = 
                        new BufferedReader(new FileReader(file));
                    
                    while (input.ready()) {
                        String line = input.readLine();
                        System.out.println(line);
                    }
                    
                    input.close();
                    
                } catch(IOException e) {
                    System.out.println("Could not open file " + path);
                }
                
                // Escribir el archivo
                try {
                    PrintWriter output = 
                        new PrintWriter(new FileWriter(file, true));
                    
                    output.println("Hello");
                    output.println("World!");
                    output.println("(Files)");
                    
                    output.close();
                    
                } catch (IOException e) {
                    System.out.println("Could not open file " + path);
                }
            }
            
        } else {
            System.out.println("Could not find " + path);
        }
        
    }
    
}
