/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.exceptions;

/**
 *
 * @author L03023498
 */
public class AgeInputException extends Exception {
    
    private int minAge;
    private int maxAge;
    private int age;
    
    public AgeInputException(String msg, int minAge, int maxAge, int age) {
        super(msg);
        
        this.minAge = minAge;
        this.maxAge = maxAge;
        this.age = age;
    }
    
    public int getMinAge() {
        return minAge;
    }
    
    public int getMaxAge() {
        return maxAge;
    }
    
    public int getAge() {
        return age;
    }
    
    @Override
    public String getMessage() {
        String msg = super.getMessage();
        return msg + "\nAge was " + age + ", but it should be between range [" + 
                minAge + ", " + maxAge + "]";
    }
}
