/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.dependencies;

/**
 *
 * @author L03023498
 */
public class Window {
    
    private Point position;
    private Point size;
    
    public Window(int x, int y, int width, int height) {
        position = new Point(x, y);
        size = new Point(width, height);
    }
    
    @Override
    public String toString() {
        return "Window: position(" + position.getX() + ", " + 
                position.getY() + ") size(" + size.getX() + 
                ", " + size.getY() + ")";
    }
    
}
