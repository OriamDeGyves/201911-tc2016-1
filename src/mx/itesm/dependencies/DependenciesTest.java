/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.dependencies;

/**
 *
 * @author L03023498
 */
public class DependenciesTest {
    
    public static void main(String[] args) {
        Author david = new Author("David Kushner", "david@kushner.com", 'm');
        
        Book mastersOfDoom = new Book("Masters of Doom", david, 400.50);
        
        System.out.println(mastersOfDoom);
        
        Window messenger = new Window(50, 50, 800, 400);
        System.out.println(messenger);
    }
    
}
