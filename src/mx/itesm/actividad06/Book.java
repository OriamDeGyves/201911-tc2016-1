/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad06;

/**
 *
 * @author L03023498
 */
public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qty;
    
    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }
    
    public Book(String name, Author[] authors, double price, int qty) {
        this(name, authors, price);
        this.qty = qty;
    }
    
    public String getName() {
        return name;
    }
    
    public Author[] getAuthors() {
        return authors;
    }
    
    public double getPrice() {
        return price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public int getQty() {
        return qty;
    }
    
    public void setQty(int qty) {
        this.qty = qty;
    }
    
    @Override
    public String toString() {
        String authorsString = "";
        if (authors.length > 0) {
            authorsString += authors[0];
        }
        for (int i = 1; i < authors.length; i++) {
            authorsString += ", " + authors[i];
        }
        
        return "Book[name = " + name + ", authors = {" + authorsString + "}, price = " + price + ", qty = " + qty + "]";
    }
    
    public String getAuthorNames() {
        if (authors.length <= 0) {
            return "";
        }
        
        String result = authors[0].getName();
        
        for (int i = 1; i < authors.length; i++) {
            result += ", " + authors[i].getName();
        }
        
        return result;
    }
}
