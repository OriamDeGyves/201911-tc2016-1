/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad03;

/**
 *
 * @author L03023498
 */
public class Cylinder extends Circle {
    private double height;
    
    public Cylinder() {
        super();
        this.height = 1.0;
    }
    
    public Cylinder(double radius) {
        super(radius);
        this.height = 1.0;
    }
    
    public Cylinder(double radius, double  height) {
        super(radius);
        this.height = height;
    }
    
    public Cylinder(double radius, double height, String color) {
        super(radius, color);
        this.height = height;
    }
    
    public double getHeight() {
        return height;
    }
    
    public void setHeight(double height) {
        this.height = height;
    }
    
    @Override
    public String toString() {
        return "Cylinder [radius = " + getRadius() + ", height = " + height + ", color = " + getColor() + "]";
    }
    
    @Override
    public double getArea() {
        return 2.0 * Math.PI * getRadius() * height + 2.0 * super.getArea();
    }
    
    public double getVolume() {
        return super.getArea() * height;
    }
}
