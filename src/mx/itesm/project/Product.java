/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.project;

/**
 *
 * @author L03023498
 */
public class Product {
    
    private final int id;
    private float price;
    
    public Product(int id, float price) {
        this.id = id;
        this.price = 0.1f;
        setPrice(price);
    }
    
    public int getId() {
        return id;
    }
    
    public void setPrice(float price) {
        if (price > 0.0f) {
            this.price = price;
        }
    }
    
    public float getPrice() {
        return price;
    }
    
    public String toString() {
        return "Product [" + id + ": " + price + "]";
    }
}
