/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.project;

/**
 *
 * @author L03023498
 */
public class Stock {
    
    private Product[] products;
    
    private int productsInStock;
    
    public Stock() {
        this(20);
    }
    
    public Stock(int size) {
        products = new Product[size];
        
        productsInStock = 0;
    }
    
    public void addToStock(Product product) throws FullStockException {
        if (productsInStock == products.length) {
            throw new FullStockException();
        }
        
        for (int i = 0; i < products.length; i++) {
            if (products[i] == null) {
                products[i] = product;
                productsInStock++;
                break;
            }
        }
    }
    
    public Product removeFromStock(int id) throws EmptyStockException {
        if (productsInStock == 0) {
            throw new EmptyStockException();
        }
        
        Product result = null;
        for (int i = 0; i < products.length; i++) {
            if (products[i] != null && products[i].getId() == id) {
                result = products[i];
                products[i] = null;
                productsInStock--;
            }
        }
        
        return result;
    }
    
    public Product getProduct(int id) {
        Product result = null;
        for (Product product : products) {
            if (product != null && product.getId() == id) {
                result = product;
            }
        }
        
        return result;
    }
    
    public Product[] getProducts() {
        Product[] result = new Product[productsInStock];
        int count = 0;
        for (Product product : products) {
            if (product != null) {
                result[count++] = product;
            }
        }
        return result;
    }
    
    public Product[] getProducts(float minPrice, float maxPrice) {
        int productsInRange = 0;
        for (Product product : products) {
            if (product == null){
                continue;
            }
            float productPrice = product.getPrice();
            if (productPrice >= minPrice && productPrice <= maxPrice) {
                productsInRange++;
            }
        }
        
        Product[] result = new Product[productsInRange];
        int count = 0;
        for (Product product : products) {
            if (product == null) {
                continue;
            }
            float productPrice = product.getPrice();
            if (productPrice >= minPrice && productPrice <= maxPrice) {
                result[count++] = product;
            }
        }
        
        return result;
    }
    
    public Dairy[] getDairyProducts() {
        int dairyProducts = 0;
        for (Product product : products) {
            if (product instanceof Dairy) {
                dairyProducts++;
            }
        }
        
        Dairy[] result = new Dairy[dairyProducts];
        int count = 0;
        for (Product product : products) {
            if (product instanceof Dairy) {
                result[count++] = (Dairy)product;
            }
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        
        if (productsInStock <= 0) {
            return "[]";
        }
        
        String result = "[";
        int count = 0;
        for (Product product : products) {
            if (product != null) {
                result += product;
                count++;
                if (count < productsInStock) {
                    result += " ";
                }
            }
        }
        
        return result;
    }
}
