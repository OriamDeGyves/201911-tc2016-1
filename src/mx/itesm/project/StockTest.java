/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.project;

/**
 *
 * @author L03023498
 */
public class StockTest {
    
    public static void main(String[] args) {
        
        Product lala = new Milk(1, 25.0f);
        Product alpura = new Milk(5, 27.90f);
        Product esmeralda = new Cheese(3, 35.0f);
        Product sanRafael = new Ham(15, 25.0f);
        Product greatValue = new Sausage(8, 30.0f);
        Product campoFrio = new Sausage(16, 27.0f);
        
        
        Stock stock = new Stock(5);
        
        try {
            stock.addToStock(lala);
            stock.addToStock(alpura);
            stock.addToStock(esmeralda);
            stock.addToStock(sanRafael);
            stock.addToStock(greatValue);
            stock.addToStock(campoFrio);
        } catch (FullStockException e) {
            System.out.println("Stock is full\n");
        }
        
        System.out.println("Products in stock: " + stock);
        
        System.out.println("");
        
        System.out.println("Search products in price range [26.0f, 30.0f]: ");
        Product[] priceResults = stock.getProducts(26.0f, 30.0f);
        for(Product product : priceResults) {
            System.out.println(product);
        }
        
        System.out.println("");
        
        System.out.println("Serach dairy products: ");
        Dairy[] dairyResults = stock.getDairyProducts();
        for (Dairy product : dairyResults) {
            System.out.println(product);
        }
        
        System.out.println("");
        
        System.out.println("Remove dairy product with id: " + dairyResults[0].getId());
        try {
            stock.removeFromStock(dairyResults[0].getId());
        } catch (EmptyStockException e) {
            System.out.println("Stock is empty\n");
        }
        System.out.println("Products in stock: " + stock);
        
        
        System.out.println("");
        
        System.out.println("Adding Sausage Campo Frio");
        try {
            stock.addToStock(campoFrio);
        } catch(FullStockException e) {
            System.out.println("Stock is full\n");
        }
        System.out.println("Products in stock: " + stock);
        
        
    }
    
}
