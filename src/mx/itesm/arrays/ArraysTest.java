/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.arrays;

// import mx.itesm.dependencies.*; // Importa todas las clases en el paquete
import mx.itesm.dependencies.Author; // Importa solo esta clase

/**
 *
 * @author L03023498
 */
public class ArraysTest {
    
    public static void main(String[] args) {
        
        Author[] authors = new Author[5];
        authors[0] = new Author("Felipe", "a@a.mx", 'm');
        authors[1] = new Author("Lucia", "b@b.mx", 'f');
        authors[2] = new Author("Jose", "c@c.mx", 'u');
        authors[3] = new Author("Maria", "d@d.mx", 'u');
        authors[4] = new Author("Ivan", "e@e.mx", 'u');
        Arrays.printArray(authors);
        
        double[] nums = { 5.0, 3.0, 6.0, 7.0, 10.0 };
        double result = Arrays.sumArray(nums);
        System.out.println("La suma de los digitos de nums es " + result);
        
        Arrays arrays = new Arrays();
        double[] grades = { 10.0, 9.0 };
        double average = arrays.averageArray(grades);
        System.out.println("El promedio de calificiaciones es " + average);
        
        int[] integers = { 5, 7, -3, 10, 0, 8 };
        System.out.println(arrays.indexOf(integers, 10));
        System.out.println(arrays.indexOf(integers, 11));
        System.out.println(arrays.indexOf(integers, -3));
    }
    
}
