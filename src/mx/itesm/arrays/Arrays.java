/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.arrays;

/**
 *
 * @author L03023498
 */
public class Arrays {
    
    public static void printArray(Object[] arr) {
        if (arr.length == 0) {
            System.out.println("[]");
            return;
        }
        
        String result = "[" + arr[0];
        for (int i = 1; i < arr.length; i++) {
            result += ", " + arr[i];
        }
        result += "]";
        
        System.out.println(result);
    }
    
    public static double sumArray(double[] arr) {
        double result = 0.0;
        for (double element : arr) {
            result += element;
        }
        return result;
    }
    
    
    public double averageArray(double[] arr) {
        return (arr.length == 0) ? 0.0 : sumArray(arr) / (double) arr.length;
    }
    
    public int indexOf(int[] arr, int e) {
        // Recorrer todos los elementos de arr
        // Por cada elemento, preguntar si es igual a e
        // En caso positivo, regresar el indice del for
        // En caso negativo, continuar
        // Si ya revisamos todos los elementos, regresamos -1
        
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == e) {
                return i;
            }
        }
        
        return -1;
    }
}
