/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.examen2a;

/**
 *
 * @author L03023498
 */
public class FuturamaTest {
    
    public static void main(String[] args) {
        
        final int UNIVERSE_ID = 137;
        
        Human farnsworth = new Farnsworth(UNIVERSE_ID);
        System.out.println("Farnsworth is alive? " + farnsworth.isAlive());
        System.out.println("Farnsworth is fighting agains aliens...");
        farnsworth.attack();
        farnsworth.flee();
        
        Profesor profesorFarnsworth = (Profesor)farnsworth;
        System.out.println("Profesor Farnsworth has published " + profesorFarnsworth.getPublishedPapers() + " papers.");
        
        Farnsworth realFarnsworth = (Farnsworth)farnsworth;
        realFarnsworth.research();
        System.out.println("Real Farnsworth, from universe " + realFarnsworth.getHomeWorld() + ", has published " + realFarnsworth.getPublishedPapers() + " papers");
        realFarnsworth.sleep();
        
        Human fry = new Fry(UNIVERSE_ID);
        System.out.println("Fry is alive? " + fry.isAlive());
        fry.attack();
        
        Fry realFry = (Fry)fry;
        realFarnsworth.setFry(realFry);
        
        realFarnsworth.attack();
        realFarnsworth.sleep();
        realFarnsworth.flee();
        realFry.flee();
        farnsworth.die();
        realFry.die();
        
        System.out.println("Farnsworth is alive? " + farnsworth.isAlive());
        System.out.println("Fry is alive? " + fry.isAlive());   
    }
}
