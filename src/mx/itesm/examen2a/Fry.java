/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.examen2a;

/**
 *
 * @author L03023498
 */
public class Fry extends Human {
    
    private final int HOMEWORLD;
    
    public Fry(int homeWorld) {
        super();
        
        this.HOMEWORLD = homeWorld;
    }
    
    public int getHomeWorld() {
        return this.HOMEWORLD;
    }
    
    @Override
    public void attack() {
        flee();
    }
    
    @Override
    public void flee() {
        System.out.println("FRY (" + HOMEWORLD + "): I don't wanna die Profesor...");
    }
    
    @Override
    public void die() {
        System.out.println("FRY (" + HOMEWORLD + "): Aughhhh");
        this.alive = false;
    }
}