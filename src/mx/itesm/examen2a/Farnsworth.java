/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.examen2a;

/**
 *
 * @author L03023498
 */
public class Farnsworth extends Profesor {
    private final int HOMEWORLD;
    private SpaceShip mySpaceShip;
    private Fry fry;
    
    public Farnsworth(int homeWorld) {
        super();
        this.HOMEWORLD = homeWorld;
        this.mySpaceShip = new SpaceShip(homeWorld);
        fry = null;
    }
    
    public int getHomeWorld() {
        return this.HOMEWORLD;
    }
    
    public void setFry(Fry fry) {
        System.out.println("FARNSWORTH (" + HOMEWORLD + "): Fry, You have a package to deliver. Let’s go on a mission.");
        this.fry = fry;
    }
    
    public String sleep() {
        return "But, I am already in my pajamas...";
    }
    
    @Override
    public void attack() {
        if (fry != null) {
            System.out.println("FARNSWORTH (" + HOMEWORLD + "): Fight, Fry!");
            fry.attack();
        } else {
            System.out.println("FARNSWORTH (" + HOMEWORLD + "): Take this, you...");
            mySpaceShip.landing();
        }
    }
    
    @Override
    public void flee() {
        System.out.println("FARNSWORTH (" + HOMEWORLD + "): Run!!!");
        mySpaceShip.takeOff();
    }
    
    @Override
    public void die() {
        if (fry != null) {
            System.out.println("FARNSWORTH (" + HOMEWORLD + "): Save yourself, Fry!");
        } else {
            System.out.println("FARNSWORTH (" + HOMEWORLD + "): I will return...");
        }
        
        this.mySpaceShip = null;
        this.alive = false;
    }
    
    @Override
    public void research() {
        publishedPapers++;
    }
}