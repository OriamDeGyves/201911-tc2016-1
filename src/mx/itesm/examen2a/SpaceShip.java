/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.examen2a;

/**
 *
 * @author L03023498
 */
public class SpaceShip {
    
    private final int HOMEWORLD;
    
    public SpaceShip(int homeWorld) {
        this.HOMEWORLD = homeWorld;
    }
    
    public int getHomeWorld() {
        return HOMEWORLD;
    }
    
    public void takeOff() {
        System.out.println("SpaceShip (" + HOMEWORLD + "): Take off in 3, 2, 1...");
    }
    
    public void landing() {
        System.out.println("SpaceShip (" + HOMEWORLD + "): Adjust your seatbelts, we are about to land...");
    }
}
