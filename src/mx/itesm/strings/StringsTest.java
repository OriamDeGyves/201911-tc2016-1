/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.strings;

import java.util.Random;

/**
 *
 * @author L03023498
 */
public class StringsTest {
    
    public static final String[] ARTICULOS = 
    {"el", "la", "ningun", "algun", "un", "una"};
    
    public static final String[] SUJETOS = 
    {"hombre", "perro", "niño", "doctor", "auto"};
    
    public static final String[] VERBOS = 
    {"manejó", "saltó", "corrió", "caminó", "brincó"};
    
    public static final String[] PREPOSICIONES =
    {"a", "desde", "sobre", "con", "en", "de", "hacia"};
    
    public static String reverse(String source) {
        StringBuffer result = new StringBuffer("");
        
        // Recorrer el String source desde el último elemento
        // hasta el primer elemento (el for va en reversa)
        for (int i = source.length() - 1; i >= 0; i--) {
            // Cada caracter que recorremos lo agregamos al StringBuffer
            result.append(source.charAt(i));
        }
        
        return result.toString();
    }
    
    
    public static String reversePhrase(String source) {
        StringBuffer result = new StringBuffer("");
        
        String[] words = source.split(" ");
        
        if (words.length > 0) {
            result.append(reverse(words[0]));
        }
        
        for (int i = 1; i < words.length; i++) {
            result.append(" ");
            result.append(reverse(words[i]));
        }
        
        return result.toString();
    }
    
    public static String phraseGenerator() {
        // Genera una frase aleatoria utilizando elementos aleatorios
        // de los arreglos:
        // Articulo Sujeto Verbo Preposicion Articulo Sujeto
        
        Random rand = new Random();
        
        StringBuffer result = new StringBuffer("");
        
        result.append (ARTICULOS[rand.nextInt(ARTICULOS.length)]);
        result.append(" ");
        result.append (SUJETOS[rand.nextInt(SUJETOS.length)]);
        result.append(" ");
        result.append (VERBOS[rand.nextInt(VERBOS.length)]);
        result.append(" ");
        result.append (PREPOSICIONES[rand.nextInt(PREPOSICIONES.length)]);
        result.append(" ");
        result.append (ARTICULOS[rand.nextInt(ARTICULOS.length)]);
        result.append(" ");
        result.append (SUJETOS[rand.nextInt(SUJETOS.length)]);
        
        return result.toString();
    }
    
    // phrase: Yo solo se que no se nada
    // oldWord: se
    // newWord: vi
    // regresa: Yo solo vi que no vi nada
    public static String changeWord(String phrase, String oldWord, String newWord) {
        StringBuffer result = new StringBuffer("");
        
        String[] words = phrase.split(" ");
        
        if (words.length > 0) {
            if (words[0].equals(oldWord)) {
                result.append(newWord);
            } else {
                result.append(words[0]);
            }
        }
        
        for (int i = 1; i < words.length; i++) {
            result.append(" ");
            if (words[i].equals(oldWord)) {
                result.append(newWord);
            } else {
                result.append(words[i]);
            }
        }
        
        return result.toString();
    }
    
    // phrase: Yo solo se que no se nada
    // word: se
    // return: Yo solo que no nada
    public static String deleteWord(String phrase, String word) {
        StringBuffer result = new StringBuffer("");
        
        String[] words = phrase.split(" ");
        
        for (int i = 0; i < words.length; i++) {
            if (!words[i].equals(word)) {
                result.append(words[i]);
                
                if (i != words.length - 1) {
                    result.append(" ");
                }
            }
        }
        
        return result.toString();
    }
    
    public static void main(String[] args) {
        
        String c = "casa";
        String r = reverse(c); // Debe regresar asac
        System.out.println(r); // Imprime asac
        
        String quote = "Yo solo se que no se nada";
        String[] words = quote.split(" ");
        for (String word : words) {
            System.out.println(word);
        }
        
        r = reversePhrase(quote); // Regresa: oY olos es euq on es adan
        System.out.println(r); // Imprime: oY olos es euq on es adan
        
        // Puede regresar: Una doctor corrió con la niño
        String phrase1 = phraseGenerator();
        System.out.println(phrase1);
        // Puede regresar: Algun hombre soltó a ningún doctor
        String phrase2 = phraseGenerator();
        System.out.println(phrase2);
        
        System.out.println(changeWord(quote, "se", "vi"));
        System.out.println(deleteWord(quote, "se"));
    }
    
}
