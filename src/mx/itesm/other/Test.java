/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.other;

/**
 *
 * @author L03023498
 */
public class Test {
    
    public static void main(String[] args) { 
        
        int[] intArr = new int[4];
        
        for (int i = 0; i < intArr.length; i++) {
            System.out.println(intArr[i]);
        }
        
        String[] strArr = new String[4];
        
        for (int i = 0; i < strArr.length; i++) {
            System.out.println(strArr[i]);
        }
        
        // Lo siguiente arroja una excepción
        /*if (strArr[0].equals("Hola")) { 
            System.out.println("Bye");
        }*/
        
        int[] intArr2 = {21, 7, 32, 25};
        System.out.println(intArr2.length);
        
        int rows = 2;
        int columns = 3;
        int[][] matrix = new int[rows][columns];
        
        System.out.println("");
        System.out.println(matrix[0].length);
        System.out.println(matrix.length);
        
        for (int i = 0; i < matrix.length; i++) { 
            for (int j = 0; j < matrix[i].length; j++) {
                //matrix[i][j] = ++value;
                matrix[i][j] = i * matrix[i].length + j;
            }
        }
        
        for (int i = 0; i < matrix.length; i++) { 
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
        
        /*System.out.println("Using java.util.Arrays.toString");
        String matStr = java.util.Arrays.toString(matrix);
        System.out.println(matStr);*/
        
        System.out.println("");
        int[] uniMatrix = new int[6];
        for (int i = 0; i < uniMatrix.length; i++) { 
            uniMatrix[i] = 10 + i * i;
        }
        int row = 0;
        int column = 2;
        int index = row * 3 + column;
        System.out.println(uniMatrix[index]);
        
        //int[][][] matrix3D = new int[rows][columns][5];
        
        
        for (int i = 1; i < 10; i++) { 
            boolean shouldBreak = false;
            for (int j = 1; j < 10; j++) {
                if (i * j == 0) {
                    shouldBreak = true;
                    break;
                }
                System.out.print(" " + i * j);
            }
            
            if (shouldBreak) {
                break;
            }
        }
    }
    
}
