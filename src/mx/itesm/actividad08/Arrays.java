/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad08;

/**
 *
 * @author L03023498
 */
public class Arrays {
    
    public static void printArray(Object[] arr) {
        if (arr.length == 0) {
            System.out.println("[]");
            return;
        }
        
        String result = "[" + arr[0];
        for (int i = 1; i < arr.length; i++) {
            result += ", " + arr[i];
        }
        result += "]";
        
        System.out.println(result);
    }
    
    public static double sumArray(double[] arr) {
        double result = 0.0;
        for (double element : arr) {
            result += element;
        }
        return result;
    }
    
    
    public double averageArray(double[] arr) {
        return (arr.length == 0) ? 0.0 : sumArray(arr) / (double) arr.length;
    }
    
    public int indexOf(int[] arr, int e) {
        // Recorrer todos los elementos de arr
        // Por cada elemento, preguntar si es igual a e
        // En caso positivo, regresar el indice del for
        // En caso negativo, continuar
        // Si ya revisamos todos los elementos, regresamos -1
        
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == e) {
                return i;
            }
        }
        
        return -1;
    }
    
    public double min(double[] arr) {
        if (arr.length <= 0) {
            return 0;
        }
        
        double minValue = arr[0];
        
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue) {
                minValue = arr[i];
            }
        }
        
        return minValue;
    }
    
    public double max(double[] arr) {
        if (arr.length <= 0) {
            return 0;
        }
        
        double maxValue = arr[0];
        
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxValue) {
                maxValue = arr[i];
            }
        }
        
        return maxValue;
    }
    
    public int[] findDuplicates(int[] arr) {
        // Create an array to identify the elements that have
        // been iterated already. This is helpfull to make
        // sure that you only count an element only once.
        // All elements start as false to indicate that
        // the array has not been iterated already.
        boolean[] iteratedElements = new boolean[arr.length];
        for (int i = 0; i < iteratedElements.length; i++) {
            iteratedElements[i] = false;
        }

        // Find duplicate count using the iteratedElements array.
        int duplicateCount = 0;
        for (int i = 0; i < arr.length; i++) {
            boolean containsDuplicates = false;
            for (int j = i + 1; j < arr.length; j++) {
                // If the elements are equal and we have not seen arr[j] before.
                if (arr[i] == arr[j] && iteratedElements[j] == false) {
                    containsDuplicates = true;
                    iteratedElements[j] = true;
                    duplicateCount++;
                }
            }
            
            // Mark arr[i] as iterated
            iteratedElements[i] = true;
            if (containsDuplicates == true) {
                // If it was a duplicated element, add 1 to the duplicate count
                // because so far we only counted the duplicates, not the original.
                duplicateCount++;
            }
        }
        
        // Create an array of duplicateCount length
        int[] duplicates = new int[duplicateCount];
        
        // Reset iterated elements
        for (int i = 0; i < iteratedElements.length; i++) {
            iteratedElements[i] = false;
        }
        
        // Find and add duplicate elements to the new array.
        int duplicatesIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            boolean containsDuplicates = false;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j] && iteratedElements[j] == false) {
                    containsDuplicates = true;
                    iteratedElements[j] = true;
                    duplicates[duplicatesIndex] = arr[j];
                    duplicatesIndex++;
                }
            }
            iteratedElements[i] = true;
            if (containsDuplicates == true) {
                duplicates[duplicatesIndex] = arr[i];
                duplicatesIndex++;
            }
        }
        
        return duplicates;
    }
    
    public int[] findCommon(int[] arr1, int[] arr2) {
        // Array to identify the common elements in arr1
        boolean[] commonInArr1 = new boolean[arr1.length];
        for (int i = 0; i < commonInArr1.length; i++) {
            commonInArr1[i] = false;
        }
        
        // Array to identify the common elements in arr2
        boolean[] commonInArr2 = new boolean[arr2.length];
        for (int i = 0; i < commonInArr2.length; i++) {
            commonInArr2[i] = false;
        }
        
        // Identify the common elements in arr1
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    commonInArr1[i] = true;
                    break;
                }
            }
        }
        // Identify the common elements in arr2
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr1.length; j++) {
                if (arr2[i] == arr1[j]) {
                    commonInArr2[i] = true;
                    break;
                }
            }
        }
        
        // Count the number of common elements in arr1
        int commonCountArr1 = 0;
        for (int i = 0; i < commonInArr1.length; i++) {
            if (commonInArr1[i] == true) {
                commonCountArr1++;
            }
        }
        
        // Count the number of common elements in arr2
        int commonCountArr2 = 0;
        for (int i = 0; i < commonInArr2.length; i++) {
            if (commonInArr2[i] == true) {
                commonCountArr2++;
            }
        }
        
        // Create an array of common elements
        int[] commonElements = new int[commonCountArr1 + commonCountArr2];
        
        // Copy the common elements of arr1 to the new array
        int commonElementsIndex = 0;
        for (int i = 0; i < arr1.length; i++) {
            if (commonInArr1[i] == true) {
                commonElements[commonElementsIndex] =  arr1[i];
                commonElementsIndex++;
            }
        }
        
        for (int i = 0; i < arr2.length; i++) {
            if (commonInArr2[i] == true) {
                commonElements[commonElementsIndex] = arr2[i];
                commonElementsIndex++;
            }
        }
        
        return commonElements;
    }
}
