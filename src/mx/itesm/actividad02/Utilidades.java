/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad02;

/**
 *
 * @author L03023498
 */
public class Utilidades {
    
    public static double calorias(String actividad, int tiempo, double peso) {
        int mets = 0;
        
        if (actividad.equals("Correr")) {
            mets = 10;
        } else if (actividad.equals("Basquetbol")) {
            mets = 9;
        } else if (actividad.equals("Tenis")) {
            mets = 4;
        } else if (actividad.equals("Bailar")) {
            mets = 3;
        } else if (actividad.equals("Caminar") || actividad.equals("Cocinar")) {
            mets = 2;
        } else if (actividad.equals("Dormir")) {
            mets = 1;
        }
        
        return 0.0175 * (double)mets * peso * (double)tiempo;
    }
    
    public static double temperatura(double celsius) {
        return (9.0 / 5.0) * celsius + 32.0;
    }
    
    public static void cuadratica(int a, int b, int c) {
        if (a == 0) {
            System.out.println("Datos incorrectos. No es posible dividir entre 0");
            return;
        }
        
        double terminoRaiz = b * b - 4 * a * c;
        
        if (terminoRaiz < 0) {
            System.out.println("Datos incorrectos. El valor dentro de la raiz debe ser mayor o igual a 0.");
            return;
        }
        
        double raiz = Math.sqrt(terminoRaiz);
        double resultado1 = (-b + raiz) / (2.0 * a);
        double resultado2 = (-b - raiz) / (2.0 * a);
        
        System.out.println("(" + resultado1 + ", " + resultado2 + ")");
    }
    
    public static double progresion(int a, int b) {
        int n = (b - a) + 1;
        return ((a + b) * n) / 2.0;
    }
    
    public static double sueldoFinal(double sueldo, double prestaciones, double tope) {
        double impuesto = 0.0;
        
        if ((sueldo + prestaciones) >= tope) {
            impuesto = 0.35;
        } else {
            impuesto = 0.2;
        }
        
        double ingresoFinal = (sueldo + prestaciones) * (1.0 - impuesto);
        
        if (prestaciones < (sueldo / 2)) {
            ingresoFinal *= 1.05;
        }
        
        return ingresoFinal;
    }
}
