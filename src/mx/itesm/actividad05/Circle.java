/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.actividad05;

/**
 *
 * @author L03023498
 */
public class Circle extends Shape {
    protected double radius;
    
    public Circle() {
        super();
        this.radius = 1.0;
    }
    
    public Circle(double radius) {
        super();
        this.radius = radius;
    }
    
    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }
    
    public double getRadius() {
        return radius;
    }
    
    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
    
    @Override
    public double getPerimeter() {
        return Math.PI * 2 * radius;
    }
    
    @Override
    public String toString() {
        return "Circle [radius = " + radius + ", color = " + color + ", filled = " + filled;
    }
}
