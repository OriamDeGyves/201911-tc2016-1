/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia01;

/**
 *
 * @author L03023498
 */
public class Gerente extends Empleado {
    
    private int empleadosASuCargo;
    
    public Gerente(String nombre, float sueldo, String nomina) {
        super(nombre, sueldo, nomina);
        
        empleadosASuCargo = 0;
    }
    
    public void agregarSubordinado() {
        empleadosASuCargo++;
    }
    
    @Override
    public String toString() {
        return "Gerente con nomina " + nomina + 
                ": " + nombre + ". Tiene " + empleadosASuCargo + " empleados" +
                " a su cargo";
    }
}
