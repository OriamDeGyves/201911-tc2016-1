/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia01;

/**
 *
 * @author L03023498
 */
public class HerenciaTest {
    
    public static void main(String[] args) {           
        Empleado oriam1 = new Empleado("Oriam", 10.0f, "L01");
        Empleado oriam2 = new Empleado("Oriam", 10.0f, "L01");
        
        System.out.println("Inician las pruebas");
        if (oriam1 == oriam2) {
            System.out.println("1.1 Las referencias son las mismas.");
            System.out.println("    Las referencias oriam1 y oriam2 apuntan al mismo objeto.");
        }
        
        if (oriam1.equals(oriam2)) {
            System.out.println("1.2 Los objetos tienen el mismo estado.");
            System.out.println("    oriam1 y oriam2 son equivalentes.");
        }
        
        System.out.println();
        
        Empleado oriam3 = oriam1;
        
        if (oriam1 == oriam3) {
            System.out.println("2.1 Las referencias son las mismas.");
            System.out.println("    Las referencias oriam1 y oriam3 apuntan al mismo objeto.");
        }
        
        if (oriam1.equals(oriam3)) {
            System.out.println("2.2 Los objetos tienen el mismo estado.");
            System.out.println("    oriam1 y oriam3 son equivalentes.");
        }
        
        System.out.println("Terminaron las pruebas");
        
        int[] intCollection = new int[5];
        for (int i : intCollection){
            i = 10;
        }
        
        for(int i : intCollection) {
            System.out.println(i);
        }
    }
    
}
