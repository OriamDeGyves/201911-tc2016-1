/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia02;

/**
 *
 * @author L03023498
 */
public abstract class Mascota extends Animal {
    
    private String color = "red";
    
    public abstract void jugar();
    
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    @Override
    public boolean equals(Object other) {
        
        if (other.getClass() == getClass()) {
            Mascota pet = (Mascota)other;
            
            return pet.color.equals(color);
        }
        
        return false;
    }
}
