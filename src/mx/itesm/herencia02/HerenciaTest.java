/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia02;

/**
 *
 * @author L03023498
 */
public class HerenciaTest {
    
    
    public static void main(String[] args) {
        Mascota fido = new Perro();
        fido.setColor("blue");
        
        Mascota chester = new Perro();
        chester.setColor("orange");
        
        Mascota kitty = new Gato();
        kitty.setColor("blue");
        
        System.out.println("Comparing fido and chester: " + 
                fido.equals(chester));
        
        System.out.println("Comparing chester and fido: " + 
                chester.equals(fido));
        
        System.out.println("Comparing fido and kitty: " + 
                fido.equals(kitty));
        
        System.out.println("Comparing kitty and fido: " + 
                kitty.equals(fido));
    }
    
}
