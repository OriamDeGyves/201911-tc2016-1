/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.itesm.herencia02;

/**
 *
 * @author L03023498
 */
public abstract class Animal {
    private int salud = 50;
    
    public void comer() {
        salud++;
    }
    
    public abstract void sonido();
}
